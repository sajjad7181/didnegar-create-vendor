


jQuery(document).ready(function($){

    // accordion

    $('.accordion ').click(function(){
        $('.accordion  .panel').stop(true, false).slideUp();
        $(this).find('.panel').stop(true, false).slideToggle();
    });

    // swiper slider
    var swiper = new Swiper(".mySwiper", {
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    

});
